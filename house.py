class House(object):
    def __init__(self,price,zone,rooms,size,url):
        self._price = price
        self._zone = zone
        self._rooms = rooms
        self._size = size
        self._url = url

    def __str__(self):
        p = "Preço: " + '{:,}'.format(self._price) + "€"
        z = "Zona: " + str(self._zone)
        q = "Quartos: " + str(self._rooms)
        t = "Tamanho: {}m\u00b2".format(self._size)
        u = "URL: " + str(self._url)
        return p + '\n' + z + '\n' + q + '\n' + t + '\n' + u

    def __repr__(self):
        return "T" + str(self._rooms) + " " + self._zone + " " + '{:,}'.format(self._price) + "€"
        
    def get_price(self):
        return self._price

    def get_zone(self):
        return self._zone

    def get_rooms(self):
        return self._rooms

    def get_size(self):
        return self._size

    def get_url(self):
        return self._url

    def get_price_per_m2(self):
        return self.price / self._size
