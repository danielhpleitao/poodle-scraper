import requests
import re
import pandas as pd
from bs4 import BeautifulSoup
from house import House

baseurl ='https://www.remax.pt'
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.134 Safari/537.36'}

#k = requests.get('https://www.remax.pt/comprar?searchQueryState={"regionName":"Lisboa","businessType":1,"page":1,"regionID":"76","regionType":"Region1ID","sort":{"fieldToSort":"ContractDate","order":1},"mapIsOpen":false,"regionCoordinates":{"latitude":38.8404598668955,"longitude":-9.2186952991834},"regionZoom":9,"prn":"Lisboa"}').text
k = requests.get('https://www.imovirtual.com/comprar/apartamento/?locations%5B0%5D%5Bregion_id%5D=11&locations%5B1%5D%5Bregion_id%5D=15&locations%5B1%5D%5Bsubregion_id%5D=220&locations%5B2%5D%5Bregion_id%5D=15&locations%5B2%5D%5Bsubregion_id%5D=227&locations%5B3%5D%5Bregion_id%5D=15&locations%5B3%5D%5Bsubregion_id%5D=224&locations%5B4%5D%5Bregion_id%5D=15&locations%5B4%5D%5Bsubregion_id%5D=225&locations%5B5%5D%5Bregion_id%5D=15&locations%5B5%5D%5Bsubregion_id%5D=223&locations%5B6%5D%5Bregion_id%5D=15&locations%5B6%5D%5Bsubregion_id%5D=221&locations%5B7%5D%5Bregion_id%5D=15&locations%5B7%5D%5Bsubregion_id%5D=219').text
soup = BeautifulSoup(k,'html.parser')
houselist = soup.find("div",{"id" : "listContainer"}).main.find("section", {"id" : "body-container"}).find("div", {"class" : "listing"}).find("div", {"class" : "container-fluid container-fluid-sm container-fluid-sm-inset"}).find("div", {"class": "row section-listing__row"}).find("div", {"class": "col-md-content section-listing__row-content"})
n_houses = int(houselist.find("div", {"class": "before-offers clearfix hidden-xs"}).find("div", {"class": "offers-index pull-left text-nowrap"}).strong.string.replace(" ", ""))
print("Número de casas", n_houses, '\n')

houselist = houselist.find_all("article")
houses = []
for h in houselist:
    
    url = h['data-url']
    zone = re.sub("Apartamento para comprar: ", "", h.find("div", {"class": "offer-item-details"}).header.p.text)
    details = h.find("div", {"class": "offer-item-details"}).find("ul", {"class": "params"})
    price = int(re.sub(r'[ €]', '', details.find('li', {'class' : 'offer-item-price'}).string))
    rooms = re.sub("T","",details.find("li", {'class': 'offer-item-rooms'}).string)
    size = float(re.sub("[^0-9,]", "", details.find("li", {"class": "offer-item-area"}).string).replace(",","."))
    
    houses.append(House(price, zone, rooms, size, url))

for h in houses:
    print(h, '\n')


